# Secure_Jenkins_Ansible_Parameterized_Builds

println(hudson.util.Secret.decrypt("{XXX=}"))
## Contexte et Objectifs
La mise en place de la CI-CD permet aux services informatiques d'une entreprises d'accèlerer la livraison de ces équipes tout en assurant le bon fonctionnement des livraisons dans l'écosystème global. Ainsi, il arrive que des outils de CI tel get Gitlab-CI ou Jenkins soient mis en places au sein des équipes. Cependant, la mise en place rapide de tels outils peut amener à de mauvaises pratiques de cybersecurité.
En effet, Jenkins à été développer pour permettre de faciliter la mise en place de jobs se déclenchants automatiquements lors d'un changement sur différentes plateformes. Pourtant, il est égallement possible de créer des jobs paramétrés prévus pour êtres lancés ponctuellement sans aucun plugins. De plus, il est possible d'ouvrir des accès non administrateurs à des personnes faisant partie d'autres services de l'entreprise en utilisant certains plugins, afin de leurs permettres de lancer eux-même des jobs.
Or, lorsqu'on ouvre des jobs jenkins à d'autres services, on ne veut généralement pas leurs donner d'accès à la machine Jenkins. C'est pourtant ce qu'il peut arriver si on ne fait pas preuve de vigilance.
Le but de ce projet est de permettre à tous de mettre en place rapidement un environement de laboratoire permettant d'illustrer les failles que j'ai eu l'occasion de repérer dans certaines utilisations de Jenkins.

_**Durée de la mise en place** : 10 minutes_
## Description de la manipulation et des différentes étapes
_**Prérequis: il est nécessaires d'installer Vagrant Et Virtualbox sur votre machine**_

**Etapes**:
- Installer Vagrant et virtualbox
- Ouvrez un terminal
```bash
git clone https://gitlab.com/personal-stored-project/vagrants_labs/secure_jenkins_ansible_parameterized_builds.git
cd secure_jenkins_ansible_parameterized_builds
vagrant up
```
- Dans votre navigateur, rendez-vous à l'adresse http://localhost

### Virtualisation
La machines virtuel tourne sur un rocky8, avec un seul proc et 4096 Mo de ram. L'hyperviseur choisi est VirtualBox.
Vagrant box : ???

### Middleware et services
- Vagrant : Génération de l'infrastructure
- Gitlab : Gestionnaire de code source
- Ansible : Instalé sur le server Jenkins

### Stack Applicative
- Python3 : Nécessaire pour Ansible 
- Java : Nécessaire pour Jenkins
- Git : Utilisé pour la gestion de notre repos gitlab

### Vagrant repo
- Commandes pour lancer la démo du projet :
```Powershell
git clone https://gitlab.com/leonardarma/leocorp-vagrants.git
cd leocorp-vagrant
Vagrant up
```
Pour arretter la démo où la relancer :
```Powershell
Vagrant halt
vagrant reload
```
Pour détruire le projet :
```Powershell
Vagrant destroy -f
```

## Synthèse
A écrire

### Sources:
A noter