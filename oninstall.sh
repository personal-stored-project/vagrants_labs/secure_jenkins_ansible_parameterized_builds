echo "
-----Install Dependencies-----
"
dnf install java-11-openjdk.x86_64 python3 ansible -y
# wget -O /etc/yum.repos.d/jenkins.repo \
#     https://pkg.jenkins.io/redhat-stable/jenkins.repo
# rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
# dnf install jenkins -y
# systemctl start jenkins
# systemctl enable jenkins
echo "
-----Get and launch Jenkins Package-----
"
wget https://get.jenkins.io/war-stable/2.319.2/jenkins.war -nv
su vagrant -c "nohup java -Djenkins.install.runSetupWizard=false -jar jenkins.war &"
echo "
-----FireWall Open 8080-----
"
firewall-cmd --permanent --add-port=8080/tcp --zone=public
firewall-cmd --reload
sleep 20
echo "
-----Get Jenkins CLI-----
"
wget http://localhost:8080/jnlpJars/jenkins-cli.jar -nv
echo "
-----Import 3 Jenkins Views-----
"
for viewpath in /vagrant/jenkinsconf/Views/*; do
    viewfile=${pathview##*/}
    java -jar jenkins-cli.jar -s http://localhost:8080 create-view ${viewfile/'.xml'/} < $viewpath
done
echo "
-----Import 3 Jenkins jobs-----
"
for jobpath in /vagrant/jenkinsconf/Jobs/*; do
    jobfile=${jobpath##*/}
    java -jar jenkins-cli.jar -s http://localhost:8080 create-job ${jobfile/'.xml'/} < $jobpath
done
echo "
-----Install Jenkins Plugins-----
"
java -jar jenkins-cli.jar -s http://localhost:8080 install-plugin git ansible:1.1 rebuild:1.34 job-dsl:1.81 -restart
sleep 100
echo "
-----Import 1 Jenkins Credential-----
"
java -jar jenkins-cli.jar -s http://localhost:8080 create-credentials-by-xml system::system::jenkins '(global)' < /vagrant/jenkinsconf/Creds/credential.xml
